<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */

/**
 * custom option and settings
 */
function tuneeco_settings_init() {
 // register a new setting for "tuneeco" page
 register_setting( 'tuneeco', 'tuneeco_options' );

 // register a new section in the "tuneeco" page
 add_settings_section(
 'tuneeco_section_developers',
 __( 'The Matrix has you.', 'tuneeco' ),
 'tuneeco_section_developers_cb',
 'tuneeco'
 );

 // register a new field in the "tuneeco_section_developers" section, inside the "tuneeco" page
 add_settings_field(
 'tuneeco_field_pill', // as of WP 4.6 this value is used only internally
 // use $args' label_for to populate the id inside the callback
 __( 'Pill', 'tuneeco' ),
 'tuneeco_field_pill_cb',
 'tuneeco',
 'tuneeco_section_developers',
 [
 'label_for' => 'tuneeco_field_pill',
 'class' => 'tuneeco_row',
 'tuneeco_custom_data' => 'custom',
 ]
 );
}

/**
 * register our tuneeco_settings_init to the admin_init action hook
 */
add_action( 'admin_init', 'tuneeco_settings_init' );

/**
 * custom option and settings:
 * callback functions
 */

// developers section cb

// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function tuneeco_section_developers_cb( $args ) {
 ?>
 <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Follow the white rabbit.', 'tuneeco' ); ?></p>
 <?php
}

// pill field cb

// field callbacks can accept an $args parameter, which is an array.
// $args is defined at the add_settings_field() function.
// wordpress has magic interaction with the following keys: label_for, class.
// the "label_for" key value is used for the "for" attribute of the <label>.
// the "class" key value is used for the "class" attribute of the <tr> containing the field.
// you can add custom key value pairs to be used inside your callbacks.
function tuneeco_field_pill_cb( $args ) {
 // get the value of the setting we've registered with register_setting()
 $options = get_option( 'tuneeco_options' );
 // output the field
 ?>
 <select id="<?php echo esc_attr( $args['label_for'] ); ?>"
 data-custom="<?php echo esc_attr( $args['tuneeco_custom_data'] ); ?>"
 name="tuneeco_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
 >
 <option value="red" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'red', false ) ) : ( '' ); ?>>
 <?php esc_html_e( 'red pill', 'tuneeco' ); ?>
 </option>
 <option value="blue" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'blue', false ) ) : ( '' ); ?>>
 <?php esc_html_e( 'blue pill', 'tuneeco' ); ?>
 </option>
 </select>
 <p class="description">
 <?php esc_html_e( 'You take the blue pill and the story ends. You wake in your bed and you believe whatever you want to believe.', 'tuneeco' ); ?>
 </p>
 <p class="description">
 <?php esc_html_e( 'You take the red pill and you stay in Wonderland and I show you how deep the rabbit-hole goes.', 'tuneeco' ); ?>
 </p>
 <?php
}

/**
 * top level menu
 */
function tuneeco_options_page() {
 // add top level menu page
 add_menu_page(
 'Tuneeco',
 'Tuneeco Options',
 'manage_options',
 'tuneeco',
 'tuneeco_options_page_html'
 );
}

/**
 * register our tuneeco_options_page to the admin_menu action hook
 */
add_action( 'admin_menu', 'tuneeco_options_page' );

/**
 * top level menu:
 * callback functions
 */
function tuneeco_options_page_html() {
 // check user capabilities
 if ( ! current_user_can( 'manage_options' ) ) {
 return;
 }

 // add error/update messages

 // check if the user have submitted the settings
 // wordpress will add the "settings-updated" $_GET parameter to the url
 if ( isset( $_GET['settings-updated'] ) ) {
 // add settings saved message with the class of "updated"
 add_settings_error( 'tuneeco_messages', 'tuneeco_message', __( 'Settings Saved', 'tuneeco' ), 'updated' );
 }

 // show error/update messages
 settings_errors( 'tuneeco_messages' );
 ?>
 <div class="wrap">
 <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
 <form action="options.php" method="post">
 <?php
 // output security fields for the registered setting "tuneeco"
 settings_fields( 'tuneeco' );
 // output setting sections and their fields
 // (sections are registered for "tuneeco", each field is registered to a specific section)
 do_settings_sections( 'tuneeco' );
 // output save settings button
 submit_button( 'Save Settings' );
 ?>
 </form>
 </div>
 <?php
}

add_action( 'init', 'cp_change_post_object' );
// Change dashboard Posts to News
function cp_change_post_object() {
    $get_post_type = get_post_type_object('post');
    $labels = $get_post_type->labels;
        $labels->name = 'Artigos';
        $labels->singular_name = 'Artigo';
        $labels->add_new = 'Novo Artigo';
        $labels->add_new_item = 'Novo Artigo';
        $labels->edit_item = 'Editar Artigo';
        $labels->new_item = 'Artigo';
        $labels->view_item = 'Visualizar';
        $labels->search_items = 'Buscar';
        $labels->not_found = 'Nenhum Artigo encontrado';
        $labels->not_found_in_trash = 'Nenhum Artigo encontrado na lixeira';
        $labels->all_items = 'Todas as Artigos';
        $labels->menu_name = 'Artigos';
        $labels->name_admin_bar = 'Artigo';
}