<!-- Custom styles - Tuneeco - BOF -->
.m-ctr { background: #cccccc; }

.author {
    line-height: 1.25rem;
    font-size: .875rem;
    letter-spacing: -.0125rem;
    margin: 14px 0 8px;
}

.publish-date {
    font-size: .75rem;
    line-height: 1rem;
    margin: 0;
}

.ampforwp_post_pagination > p > a {
    font-size: 24pt;
}

.fsp-cnt {
	padding: 0 !important;
}

.author-publish-date{
	margin-top: 2rem;
}

.card{
	background-color: white;
	box-shadow: 0 1px 2px rgba(0,0,0,.1);
	padding: 0.1rem 20px;
	margin: 0 0 15px 0;
}

.archive {
	background-color: #eee;
}

.arch-tlt {
  margin: 0 0 30px 0;
  text-align: center;
  padding: 10px;
  text-transform: uppercase;
  font-size: 12px;
  letter-spacing: -0.3px;
}

.amp-archive-title {
  font-weight: 100;
}

.p-m-fl {
  display: none;
}

.social {
	margin: 1rem 0 0 -10px;
}

.hr-text {
  line-height: 1em;
  position: relative;
  outline: 0;
  border: 0;
  color: black;
  text-align: center;
  height: 1.5em;
  opacity: .5;
}
.hr-text:before {
  content: '';
  background: #818078;
  position: absolute;
  left: 0;
  top: 0.5rem;
  width: 100%;
  height: 1px;
}
.hr-text:after {
  content: attr(data-content);
  position: relative;
  display: inline-block;
  color: black;
  padding: 0 .5em;
  line-height: 1.5em;
  color: #818078;
  background-color: #fff;
  //font-family: Futura, sans-serif;
  text-transform: uppercase;
  font-size: 0.7rem;
}

.ampforwp_post_pagination > p > a {
/*
    background-color: #f44336;
    color: white;
    padding: 14px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    */

   text-size-adjust: 100%;
   -webkit-font-smoothing: antialiased;

   box-sizing: inherit;

   color: #e94662;
   width: 268px;
   text-align: center;
   padding: 15px;

   border: 1px solid #ebebeb;
   border-radius: 3px;
   background-color: #fff;
   font-family: opensans,sans-serif;
   font-size: 13px;
   letter-spacing: .5px;
   box-shadow: 0 1px 0 #cecece;
   display: inline-block;
   font-weight: 700;
   text-transform: uppercase;
   cursor: pointer;
}

/* fundo do menu */
.m-ctr {
  background-color: white;
}

/* caixa de busca */
.m-srch .s {
  border: 1px solid #e94662;
}

amp-ad { margin: auto }

.entry-excerpt {
  font-size: 16px;
  color: #444;
  margin-top: 10px;
  line-height: 20px;
}

.wp-caption-text {
  display: none;
}