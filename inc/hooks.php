<?php
/**
 * Hooks
 *
 * @package    tuneeco-plugin
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

function tuneeco_pageview( $page ) {
    tuneeco_db_pageview( $page );
}
