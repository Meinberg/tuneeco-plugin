<?php

global $tuneeco_db_version;
$tuneeco_db_version = '1.0';

function tuneeco_db_install() {
    // die('tuneeco_db_install');
    global $wpdb;
    global $tuneeco_db_version;

    $table_name = $wpdb->prefix . 'tuneeco_most_read';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
        effdt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        post_id BIGINT UNSIGNED NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    add_option( 'tuneeco_db_version', $tuneeco_db_version );
}

function tuneeco_db_install_data() {
    global $wpdb;

    $post_id = 1;

    $table_name = $wpdb->prefix . 'tuneeco_most_read';

    $wpdb->insert(
        $table_name,
        array(
            'post_id' => $post_id
        )
    );
}

function tuneeco_db_pageview( $post_id ) {
    global $wpdb;

    $table_name = $wpdb->prefix . 'tuneeco_most_read';

    $wpdb->insert(
        $table_name,
        array(
            'post_id' => $post_id
        )
    );
}

function tuneeco_update_db_check() {
    global $tuneeco_db_version;
    if ( get_site_option( 'tuneeco_db_version' ) != $tuneeco_db_version ) {
        tuneeco_db_install();
    }
}
add_action( 'plugins_loaded', 'tuneeco_update_db_check' );