<?php
/**
 * Cool widgets
 *
 * @package    tuneeco-plugin
 * @copyright  Copyright (c) 2020, Claudio Meinberg
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

// Creating the widget
class tuneeco_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of your widget
            'tuneeco_widget',
            // Widget name will appear in UI
            __('Tuneeco: Mais lidas', 'tuneeco_widget_domain'),
            // Widget description
            array( 'description' => __( 'Teste de widget', 'tuneeco_widget_domain' ), )
        );
    }

    // Creating widget front-end
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];

        // This is where you run the code and display the output
        echo __( 'Hello, World!', 'tuneeco_widget_domain' );
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
        }
        else {
        $title = __( 'New title', 'tuneeco_widget_domain' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }

    // Class tuneeco_widget ends here
}


require __DIR__ . '/widgets/recent_posts.php';

// Register and load the widget
function tuneeco_load_widget() {
    register_widget( 'tuneeco_widget' );
    register_widget( 'Tuneeco_Widget_Recent_Posts' );
}
add_action( 'widgets_init', 'tuneeco_load_widget' );
