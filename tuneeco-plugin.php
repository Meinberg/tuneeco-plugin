<?php
/*
Plugin Name: Tuneeco Plugin
Plugin URI: https://tunee.co/plugin/
Description: Extensão para sites foda
Version: 1.0
Author: Claudio Meinberg
Author URI: https://tunee.co/
License: GPL2
AMP: Tuneeco AMP Theme
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

require('tuneeco-plugin-settings.php');


/**
 * Widgets scripts for tuneeco plugin
 */
require __DIR__ . '/inc/widgets.php';

/**
 * Hooks for tuneeco plugin
 */
require __DIR__ . '/inc/hooks.php';

/**
 * DB functions tuneeco plugin
 */
require __DIR__ . '/inc/db.php';
register_activation_hook( __FILE__, 'tuneeco_db_install' );
register_activation_hook( __FILE__, 'tuneeco_db_install_data' );

// ampforwp_before_social_icons_hook

// add_action( 'tuneeco_social_icons', 'insert_social_icons' );
function insert_social_icons(){ ?>
    <div class="social">
        <amp-addthis
            width="320"
            height="50"
            data-pub-id="ra-5ed32a2282052007"
            data-widget-id="9gni"
            data-widget-type="inline">
        </amp-addthis>
    </div>
<?php
}



// add_action( 'ampforwp_post_after_design_elements', 'insert_ad_feeds' );
// function insert_ad_feeds() {
//     // cm_amb_teads_for_amp();
//     display_taboola_feed();
// }

// add_action( 'tuneeco_teads_ad', 'insert_teads_ad' );
// add_action( 'ampforwp_post_after_design_elements', 'insert_teads_ad' );
function insert_teads_ad(){ ?>
    <amp-ad
      width="300"
      height="1"
      type="teads"
      data-pid="96787"
      layout="responsive">
      <div placeholder></div>
      <div fallback></div>
    </amp-ad>
<?php
}

// add_action( 'ampforwp_post_after_design_elements', 'display_taboola_feed' );
function display_taboola_feed(){ ?>
    <amp-embed
      type="taboola"
      width="100"
      height="100"
      layout="responsive"
      data-publisher="gold360-anamariabraga"
      data-mode="alternating-thumbnails-c-amp"
      data-placement="Below Article Thumbnails AMP"
      data-target_type="mix"
      data-article="auto"
    />
<?php
}


// add_action( 'tuneeco_news_ad_top', 'insert_news_ad_top' );
function insert_news_ad_top(){ ?>
    <hr class="hr-text" data-content="Continua depois da publicidade">
    <div class="ad">
        <amp-ad width=336 height=280
            type="doubleclick"
            data-slot="/122331895/amb-cm/amp-news-top"
            data-multi-size="336x250,320x250,320x100,320x50,300x250,300x100"
            data-multi-size-validation="false">
          <div placeholder></div>
          <div fallback></div>
        </amp-ad>
    </div>
    <hr class="hr-text" data-content="">
<?php
}

// add_action( 'tuneeco_news_ad_sticky_footer', 'insert_news_ad_sticky_footer' );
function insert_news_ad_sticky_footer(){ ?>
    <amp-sticky-ad layout="nodisplay">
        <amp-ad width=320 height=50
            type="doubleclick"
            data-slot="/122331895/amb-cm/amp-news-sticky-footer">
        </amp-ad>
    </amp-sticky-ad>
<?php
}

// CM - ADICIONAR ESPAÇOS DE ANUNCIO
// Modify the content to includ ad units
// add_filter('ampforwp_modify_the_content','ampforwp_post_content_autoad_placing');
function ampforwp_post_content_autoad_placing($content){

    $paragraphAfter[1] = '<div>AFTER FIRST</div>'; //display after the first paragraph
    $paragraphAfter[3] = '<div>AFTER THIRD</div>'; //display after the third paragraph
    $paragraphAfter[5] = '<div>AFTER FIFtH</div>'; //display after the fifth paragraph

    // $paragraphAfter[3] = '<amp-fx-flying-carpet height="300px">
    //   <amp-ad width=336 height=280
    //       type="doubleclick"
    //       data-slot="/122331895/amb-cm/amp-news-content-1"
    //       data-multi-size="336x250,320x250,320x100,320x50,300x250,300x100"
    //       data-multi-size-validation="false">
    //   </amp-ad>
    // </amp-fx-flying-carpet>';

    // $content = apply_filters( 'the_content', get_the_content() );

    $content = explode("</p>", $content);
    $count = count($content);
    $new_content = '';
    for ($i = 0; $i < $count; $i++ ) {
        $new_content .= $content[$i] . "</p>";

        if ( array_key_exists($i, $paragraphAfter) ) {
            // echo $paragraphAfter[$i];
            $new_content .= $paragraphAfter[$i];
        }
        // echo $content[$i] . "</p>";

    }

    return $new_content;
}
// ADCIONA ESPAÇOS DE ANUNCIO

add_filter('ampforwp_post_pagination_args','redefine_next_buttons_args');
function redefine_next_buttons_args($params){
    global $redux_builder_amp;
    $defaults = array(
        'before'           => '<div class="ampforwp_post_pagination" ><p>' . '<span>' .  ampforwp_translation($redux_builder_amp['amp-translator-page-text'], 'Page') . ':</span>',
        'after'            => '</p></div>',
        'link_before'      => '',
        'link_after'       => '',
        'next_or_number'   => 'next',
        'separator'        => ' ',
        'nextpagelink'     => 'Continuar a leitura',
        'previouspagelink' => 'Página anterior',
        'pagelink'         => '%',
        'echo'             => 1
    );

    return $defaults;
}

// apply_filters( 'ampforwp_post_pagination_page', $text, $page, $numpages);
add_filter('ampforwp_post_pagination_page','redefine_pagination_pages', 10, 3);
function redefine_pagination_pages($text, $page, $numpages){
    return;
}


add_filter('ampforwp_post_pagination_link','redefine_pagination_text', 10, 2);
function redefine_pagination_text( $link, $page ){
    if(preg_match('/(.*?)Continuar a leitura(.*?)/', $link)){
        return $link;
    }
}

add_filter('wpseo_breadcrumb_single_link', 'remove_breadcrumb_title' );
function remove_breadcrumb_title( $link_output) {
    if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
        $link_output = '';
    }
    return $link_output;
}
